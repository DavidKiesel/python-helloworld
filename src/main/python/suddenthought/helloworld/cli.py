#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2020 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

import argparse
import sys

# local
from suddenthought.helloworld.greeter import Greeter


def get_parser():
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Get parser.

    :return: A parser.
    :rtype: argparse.ArgumentParser
    """

    parser = \
        argparse.ArgumentParser(
            prog='helloworld',
            formatter_class=argparse.RawDescriptionHelpFormatter,
            description="""
Hello, world.
""",
            add_help=True,
            epilog="""
Examples:

    %(prog)s

    %(prog)s --greeting GREETING --who WHO
"""
        )

    parser.add_argument(
        '--greeting',
        '-g',
        metavar='GREETING',
        default='Hello',
        help="""
greeting
"""
    )

    parser.add_argument(
        '--who',
        '-w',
        metavar='WHO',
        default='world',
        help="""
who
"""
    )

    return parser


def main(
    out=sys.stdout,
    greeting='Hello',
    who='world'
):
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Create instance of :class:`.Greeter`, get message from ``Greeter``
    instance, write message to ``out``, write newline to ``out``.

    :param out: Write message to this output.
    :type out: output stream

    :param str who: Who.
    """

    greeter = \
        Greeter(
            greeting=greeting
        )

    message = \
        greeter.get_message(
            who
        )

    out.write(
        message
    )

    out.write(
        '\n'
    )
