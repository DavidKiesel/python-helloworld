#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2020 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################


class Greeter:
    # docstring conventions;
    # https://www.python.org/dev/peps/pep-0257/
    # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
    """
    Greeter.
    """
    @staticmethod
    def get_hello_message(
        who
    ):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Get greeting to ``who``.

        :param str who: Who.

        :return: ``f'Hello, {who}.'``
        :rtype: str
        """

        return f'Hello, {who}.'

    def __init__(
        self,
        greeting='Hello'
    ):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Constructor.

        :param str greeting: A greeting.
        """

        self.greeting = greeting

    def get_message(
        self,
        who
    ):
        # docstring conventions;
        # https://www.python.org/dev/peps/pep-0257/
        # https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
        """
        Get greeting to ``who``.

        :param str who: Who.

        :return: ``f'{self.greeting}, {who}.'``
        :rtype: str
        """

        return f'{self.greeting}, {who}.'
