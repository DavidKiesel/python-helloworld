#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2020 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

from io import StringIO
import unittest
from unittest import TestCase

from suddenthought.helloworld import cli


class Test(TestCase):
    def test_get_parser__normal(self):
        ######################################################################
        # prepare

        argvs = \
            [
                [],
                ['--greeting', 'Greetings', '--who', 'John Doe']
            ]

        expected_results = \
            [
                ['Hello', 'world'],
                ['Greetings', 'John Doe']
            ]

        ######################################################################
        # test and assert

        for i in range(0, len(argvs)):
            with self.subTest(
                i=i
            ):
                parser = \
                    cli.get_parser().parse_args(
                        argvs[i]
                    )

                self.assertEquals(
                    parser.greeting,
                    expected_results[i][0]
                )

                self.assertEquals(
                    parser.who,
                    expected_results[i][1]
                )

    def test_get_parser__no_such_option(self):
        ######################################################################
        # prepare

        argvs = \
            [
                ['--no_such_option']
            ]

        expected_results = \
            [
                SystemExit
            ]

        ######################################################################
        # test and assert

        for i in range(0, len(argvs)):
            with self.subTest(
                i=i
            ):
                with self.assertRaises(expected_results[i]):
                    cli.get_parser().parse_args(
                        argvs[i]
                    )

    def test_main(self):
        ######################################################################
        # prepare

        greetings = \
            [
                'Hello',
                'Hi',
                None
            ]

        whos = \
            [
                'John',
                'John Doe',
                None
            ]

        expected_results = \
            {
                'Hello': {
                    'John': 'Hello, John.\n',
                    'John Doe': 'Hello, John Doe.\n',
                    None: 'Hello, None.\n'
                },
                'Hi': {
                    'John': 'Hi, John.\n',
                    'John Doe': 'Hi, John Doe.\n',
                    None: 'Hi, None.\n'
                },
                None: {
                    'John': 'None, John.\n',
                    'John Doe': 'None, John Doe.\n',
                    None: 'None, None.\n'
                }
            }

        ######################################################################
        # test and assert

        for greeting_index in range(0, len(greetings)):
            greeting = greetings[greeting_index]

            for who_index in range(0, len(whos)):
                who = whos[who_index]

                subtest_index = \
                    greeting_index * \
                    len(whos) + \
                    who_index

                with self.subTest(
                    i=subtest_index
                ):
                    with StringIO() as out:
                        cli.main(
                            out=out,
                            greeting=greeting,
                            who=who
                        )

                        self.assertEqual(
                            out.getvalue(),
                            expected_results[greeting][who]
                        )


if __name__ == '__main__':
    unittest.main()
