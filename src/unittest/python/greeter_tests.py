#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2020 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

import unittest
from unittest import TestCase

from suddenthought.helloworld.greeter import Greeter


class Test(TestCase):
    def test_get_hello_message(self):
        ######################################################################
        # prepare

        whos = \
            [
                'John',
                'John Doe'
            ]

        expected_results = \
            [
                'Hello, John.',
                'Hello, John Doe.'
            ]

        ######################################################################
        # test and assert

        for i in range(0, len(whos)):
            with self.subTest(
                i=i
            ):
                self.assertEqual(
                    Greeter.get_hello_message(
                        whos[i]
                    ),
                    expected_results[i]
                )

    def test_init(self):
        ######################################################################
        # prepare

        greetings = \
            [
                'Hello',
                'Hi'
            ]

        expected_results = \
            [
                'Hello',
                'Hi'
            ]

        ######################################################################
        # test and assert

        for i in range(0, len(greetings)):
            with self.subTest(
                i=i
            ):
                greeter = \
                    Greeter(
                        greeting=greetings[i]
                    )

                self.assertEqual(
                    greeter.greeting,
                    expected_results[i]
                )

    def test_get_message(self):
        ######################################################################
        # prepare

        whos = \
            [
                'John',
                'John Doe'
            ]

        expected_results = \
            [
                'Hello, John.',
                'Hello, John Doe.'
            ]

        greeter = \
            Greeter(
                greeting='Hello'
            )

        ######################################################################
        # test and assert

        for i in range(0, len(whos)):
            with self.subTest(
                i=i
            ):
                self.assertEqual(
                    greeter.get_message(
                        whos[i]
                    ),
                    expected_results[i]
                )


if __name__ == '__main__':
    unittest.main()
