#!/usr/bin/env python

#   -*- coding: utf-8 -*-

##############################################################################
# copyrights and license
#
# Copyright (c) 2020 David Harris Kiesel
#
# Licensed under the MIT License. See LICENSE in the project root for license
# information.
##############################################################################

##############################################################################
# file information
#
# https://pybuilder.io/documentation/manual
# https://gist.github.com/miebach/9752025

##############################################################################
# imports

from pybuilder.core import \
    Author, \
    init, \
    task, \
    use_bldsup, \
    use_plugin

# bldsup
# https://pybuilder.io/documentation/plugins#additional-project-structure
use_bldsup(
    build_support_dir='bldsup'
)
import build_util

##############################################################################
# plugins
# https://pybuilder.io/documentation/plugins

use_plugin('python.core')
#use_plugin('python.install_dependencies')
use_plugin('python.flake8')
use_plugin('python.unittest')
#use_plugin('python.integrationtest')
use_plugin('python.coverage')
use_plugin('python.distutils')
# https://www.sphinx-doc.org/en/master/
use_plugin('python.sphinx')

##############################################################################
# set project attributes
#
# at least some of these (e.g., version) need to be set as globals for
# PyBuilder to function properly
#
# PyBuilder automatically sets project attributes based on the globals

# sets setup.py variable name
name = 'suddenthought.helloworld'

# sets setup.py variable version
# https://packaging.python.org/guides/distributing-packages-using-setuptools/#choosing-a-versioning-scheme
# https://www.python.org/dev/peps/pep-0440/
version = '1.0.0.dev'

#default_task = 'publish'
default_task = \
    [
        'clean',
        'analyze',
        'publish',
        'sphinx_generate_documentation'
    ]

# sets setup.py variable description
summary = 'SUMMARY'

# sets setup.py variable long_description
description = 'DESCRIPTION'

# sets setup.py variable author and author_email
authors = \
    [
        Author(
            'David Harris Kiesel',
            'david@suddenthought.net'
        )
    ]

# sets setup.py variable maintainer and maintainer_email
maintainers = \
    [
        Author(
            'David Harris Kiesel',
            'david@suddenthought.net'
        )
    ]

# sets setup.py variable license
#
# https://choosealicense.com/
#
# https://spdx.org/licenses/
license = 'MIT'

# sets setup.py variable url
url = 'https://example.com/'

# sets setup.py variable project_urls
urls = \
    {
            'URLS_1': 'https://example.com/urls_1',
            'URLS_2': 'https://example.com/urls_2'
    }

# sets setup.py variable namespace_packages
# explicit_namespaces = \
#     [
#         'EXPLICIT_NAMESPACES_1',
#         'EXPLICIT_NAMESPACES_2'
#     ]

# sets setup.py variable python_requires
requires_python = '>=3.9'

# sets setup.py variable obsoletes
#obsoletes = \
#    [
#        'OBSOLETES_1',
#        'OBSOLETES_2'
#    ]

##############################################################################
# set project properties

@init
def set_properties(
    project
):
    ##########################################################################
    # flake8 plugin properties
    #
    # https://pybuilder.io/documentation/plugins

    project.set_property(
        'flake8_include_test_sources',
        True
    )

    ##########################################################################
    # coverage plugin properties
    #
    # https://pybuilder.io/documentation/plugins

#    project.set_property(
#        'coverage_break_build',
#        False
#    )

    ##########################################################################
    # distutils plugin properties
    #
    # https://pybuilder.io/documentation/plugins

    # sets setup.py variable classifiers
    # https://pypi.org/classifiers/
    project.set_property(
        'distutils_classifiers',
        [
            'Development Status :: 3 - Alpha',
            'Environment :: Console',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: MIT License',
            'Programming Language :: Python',
            'Programming Language :: Python :: 3 :: Only',
            'Programming Language :: Python :: 3.9'
        ]
    )

    ##########################################################################
    # sphinx plugin properties
    #
    # https://pybuilder.io/documentation/plugins

    project.set_property(
        'sphinx_config_path',
        'docs/source'
    )

    project.set_property(
        'sphinx_output_dir',
        'docs/build'
    )

    project.set_property(
        'sphinx_doc_author',
        'David Harris Kiesel'
    )

    project.set_property(
        'sphinx_project_name',
        project.name
    )

    project.set_property(
        'sphinx_project_version',
        project.version
    )

# https://pybuilder.io/documentation/writing-plugins
@task(
    description='Example of a custom task.'
)
def some_task(
    project,
    logger
):
    build_util.execute_some_build_function(
        project,
        logger,
        'Hello, build.'
    )
