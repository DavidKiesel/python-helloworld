Usage
=====

.. code-block:: bash

   $ helloworld --help
   usage: helloworld [-h] [--greeting GREETING] [--who WHO]

   Hello, world.

   optional arguments:
     -h, --help            show this help message and exit
     --greeting GREETING, -g GREETING
                           greeting
     --who WHO, -w WHO     who

   Examples:

       helloworld

       helloworld --greeting GREETING --who WHO
