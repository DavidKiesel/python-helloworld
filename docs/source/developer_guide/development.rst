Development
===========

Create the Repository Directory
-------------------------------

Starting in the directory in which you normally store projects, execute the
commands below, replacing the value of ``python-helloworld`` as needed.

.. code-block:: bash

   mkdir python-helloworld

   cd "$_"

   git init

   git checkout -b trunk

Note that, in git versions 2.28+, the two git commands can be replaced with a
single git command ``git init --initial-branch trunk``.

Create file ``.gitignore`` to prevent git from including extraneous files.  See
`github/gitignore <https://github.com/github/gitignore>`_.

Choosing a License
------------------

For discussions on licensing, see `Choose an open source license
<https://choosealicense.com/>`_ and `GNU licenses
<https://www.gnu.org/licenses/>`_.

For a long list of licenses with identifiers and license text, see `SPDX
License List <https://spdx.org/licenses/>`_.

Some thoughts...

If you are ever going to regret someone using your code and making a gazillion
dollars of which you will not see one dime, do not share the code.

If you are fine with someone using your code and making a gazillion dollars of
which you will not see one dime but you want to prevent them from modifying and
distributing the software without "sharing back", consider a GPL or LGPL
license.

If you are fine with someone using your code and making a gazillion dollars of
which you will not see one dime and all you want is for your license to carry
forward with respect to your code ("See, I wrote this!"), consider the MIT
license.

Another things to consider is how you would prefer to handle code contributions
from others and the legal requirements...

Applying a license...

Add the full license text to a ``LICENSE`` file in the repository directory.
This can be done right away.

GNU further recommends applying a copyright and license header to each file.
E.g.,

.. code-block:: python

   ##############################################################################
   # copyrights and license
   #
   # Copyright (c) 2020 David Harris Kiesel
   #
   # Licensed under the MIT License. See LICENSE in the project root for license
   # information.
   ##############################################################################

Set the value of the ``build.py`` project attribute ``license`` to the SPDX
license identifier.  You will have to wait to do this until after a later step
in this document.  E.g.,

.. code-block:: python

   license = 'MIT'

Add a license element to the ``build.py`` project property
``distutils_classifiers``.  See `PyPI classifiers
<https://pypi.org/classifiers/>`_.  You will have to wait to do this until
after a later step in this document.  E.g.,

.. code-block:: python

   @init
   def set_properties(
       project
   ):
       ...

       project.set_property(
           'distutils_classifiers',
           [
               'Development Status :: 3 - Alpha',
               'Environment :: Console',
               'Intended Audience :: Developers',
               'License :: OSI Approved :: MIT License',
               'Programming Language :: Python',
               'Programming Language :: Python :: 3 :: Only',
               'Programming Language :: Python :: 3.9'
           ]
       )

Set Python Version
------------------

Repository file ``.python-version`` signals to the ``pyenv`` ``python`` shim,
when executed in the repository directory or below, to use the Python version
in the file.

To see the list of currently installed Python versions available to the user in
a system location (e.g., under ``/usr/bin``) or a user location (e.g., under
``$HOME/.pyenv``), execute the command below.

.. code-block:: bash

   pyenv versions

To see the list of Python versions that could be installed, execute the command
below.

.. code-block:: bash

   pyenv install --list

To install a version of Python under ``HOME/.pyenv``, execute the command
below, replacing ``3.9.0`` as needed.

.. code-block:: bash

   pyenv install 3.9.0

To set a version of Python for the repository by creating file
``.python-version``, from the top repository directory, execute the command
below, replacing ``3.9.0`` as needed.

.. code-block:: bash

   pyenv local 3.9.0

PyBuilder Initialization
------------------------

Create Initial PyBuilder Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create initial PyBuilder files, from the repository directory, execute the
command below.  When prompted, each of the defaults should be satisfactory. 

.. code-block:: bash

   pyb --start-project

``pyb`` will have created the files below.

``build.py``
    This is the PyBuilder configuration file.

``pyproject.toml``
    Specifies build system dependencies per `PEP 518
    <https://www.python.org/dev/peps/pep-0518/>`_.

``setup.py``
    Supports installation via ``pip install git+git://<project>@<branch>`` in
    conjunction with PyBuilder.

Additionally, ``pyb`` will have created the directories below.

``src/main/python``
    Normal Python code is stored under here.
``src/main/scripts``
    Python scripts are stored under here.
``src/unittest/python``
    Python unit test code is stored under here.

Customize ``build.py``
^^^^^^^^^^^^^^^^^^^^^^

Customize ``build.py`` as needed.

* add ``import`` statements needed within ``build.py``

   * e.g.,

   .. code-block:: python

      from pybuilder.core import \
          Author, \
          init, \
          task, \
          use_bldsup, \
          use_plugin

* use `use_bldsup
  <https://pybuilder.io/documentation/plugins#additional-project-structure>`_
  to specify a directory containing additional Python modules to import for use
  within ``build.py``

   * this can be used to prevent clutter in file ``build.py``
   * e.g.,

   .. code-block:: python

      # bldsup
      # https://pybuilder.io/documentation/plugins#additional-project-structure
      use_bldsup(
          build_support_dir='bldsup'
      )
      import build_util

* add PyBuilder `plugins <https://pybuilder.io/documentation/plugins>`_

   * e.g.,

   .. code-block:: python

      use_plugin('python.core')
      #use_plugin('python.install_dependencies')
      use_plugin('python.flake8')
      use_plugin('python.unittest')
      #use_plugin('python.integrationtest')
      use_plugin('python.coverage')
      use_plugin('python.distutils')
      # https://www.sphinx-doc.org/en/master/
      use_plugin('python.sphinx')

* add project attributes as globally scoped variables

   * e.g.,

   .. code-block:: python

      name = 'suddenthought.helloworld'

* add project properties through function calls within an initializer

   * e.g.,

   .. code-block:: python

      @init
      def set_properties(
          project
      ):
          ##########################################################################
          # flake8 plugin properties
          #
          # https://pybuilder.io/documentation/plugins

          project.set_property(
              'flake8_include_test_sources',
              True
          )

* add `custom tasks <https://pybuilder.io/documentation/writing-plugins>`_ as
  globally scoped functions with the ``@task`` decorator and parameters
  ``project`` and ``logger``

   * e.g.,

   .. code-block:: python

      # https://pybuilder.io/documentation/writing-plugins
      @task(
          description='Example of a custom task.'
      )
      def some_task(
          project,
          logger
      ):
          print('Hello, build.')

Project Attributes
^^^^^^^^^^^^^^^^^^

``name`` (``str``)
    Applied to distribution ``setup.py`` variable ``name``.

``version`` (``str``)
    Applied to distribution ``setup.py`` variable ``version``.

``default_task`` (``str`` or ``list`` of ``str``)
    Default task or tasks for ``pyb`` to execute.

``summary`` (``str``)
    Applied to distribution ``setup.py`` variable ``description``.

``description`` (``str``)
    Applied to distribution ``setup.py`` variable ``long_description``.

``authors`` (``list`` of ``pybuilder.core.Author``)
    Applied to distribution ``setup.py`` variables ``author`` and
    ``author_email``.

``maintainers`` (``list`` ``pybuilder.core.Author``)
    Applied to distribution ``setup.py`` variables ``maintainer`` and
    ``maintainer_email``.

``license`` (``str``)
    Applied to distribution ``setup.py`` variable ``license``.

``url`` (``str``)
    Applied to distribution ``setup.py`` variable ``url``.

``urls`` (``dict``)
    Applied to distribution ``setup.py`` variable ``project_urls``.

``explicit_namespaces`` (``list`` of ``str``)
    Applied to distribution ``setup.py`` variable ``namespace_packages``.

``requires_python`` (``str``)
    Applied to distribution ``setup.py`` variable ``python_requires``.

``obsoletes`` (``list`` of ``str``)
    Applied to distribution ``setup.py`` variable ``obsoletes``.

References:

* `PyBuilder - Plugins <https://pybuilder.io/documentation/plugins>`_
* `core.py <https://github.com/pybuilder/pybuilder/blob/master/src/main/python/pybuilder/core.py>`_
* `reactor.py <https://github.com/pybuilder/pybuilder/blob/master/src/main/python/pybuilder/reactor.py>`_
* `Packaging namespace packages <https://packaging.python.org/guides/packaging-namespace-packages/>`_

Project Properties
^^^^^^^^^^^^^^^^^^

References:

* `PyBuilder - Plugins <https://pybuilder.io/documentation/plugins>`_

Sphinx Initialization
---------------------

Create Initial Sphinx Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^

To create initial Sphinx files, from the repository directory, execute the
commands below.  When prompted with ``Separate source and build directories``,
respond with `y`.  When prompted with ``Project name``, respond as appropriate
(e.g., ``python-helloworld``).  When prompted with ``Author name(s)``, respond
as appropriate.  When prompted with ``Project release``, respond as appropriate
(e.g., ``1.0.0.dev``).  When prompted with ``Project language``, respond with
``en``.

.. code-block:: bash

   mkdir -p docs

   (
       cd docs

       sphinx-quickstart
   )

``sphinx-quickstart`` creates the files and directories below under directory
``docs``.

* ``build``

    * directory under which Sphinx output is written

* ``source``

    * directory under which Sphinx source files are stored
    * ``conf.py``

        * Sphinx `configuration file
          <https://www.sphinx-doc.org/en/master/usage/configuration.html>`_

    * ``index.rst``

        * the `master document
          <https://www.sphinx-doc.org/en/master/glossary.html#term-master-document>`_,
          the initial documentation page, formatted as `reStructuredText
          <https://www.sphinx-doc.org/en/master/usage/restructuredtext/>`_)

    * ``_static``

        * directory under which static source files (e.g., style sheets or
          script files) are stored; contents are copied under the output's
          ``_static`` directory after the theme files

    * ``_templates``

        * directory under which HTML `template files
          <https://www.sphinx-doc.org/en/master/templating.html>`_ are stored

* ``make.bat``

    * a batch file for building documentation from Sphinx files

* ``Makefile``

    * a ``makefile`` for building documentation from Sphinx files

References:

* `Sphinx <https://www.sphinx-doc.org/en/master/>`_
* `Sphinx - reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/>`_
* `Introduction to reStructuredText <https://www.writethedocs.org/guide/writing/reStructuredText/>`_

Create Sphinx API Files
^^^^^^^^^^^^^^^^^^^^^^^

To create Sphinx API files with ``automodule`` directives based on current
source code, execute commands like the ones below, modifying the final argument
of the ``sphinx-apidoc`` command as needed.

.. code-block:: bash

   mkdir -p docs

   (
       cd docs

       sphinx-apidoc \
           --force \
           --separate \
           --implicit-namespaces \
           --module-first \
           --output-dir source/api \
           ../src/main/python/suddenthought/
   )

Add file ``docs/source/api/index.rst``.  E.g.,

.. code-block:: restructuredtext

   API
   ===

   .. toctree::
      :maxdepth: 2
      
      suddenthought.helloworld

Customize Sphinx File ``conf.py``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Modify ``Path setup`` section.  E.g.,

.. code-block:: python

   import os
   import sys

   sys.path.insert(
       0,
       os.path.abspath(
           os.path.join(
               '..',
               '..',
               'src',
               'main',
               'python'
           )
       )
   )

Add elements to ``extensions`` list as needed.  E.g.,

.. code-block:: python

   extensions = [
       'sphinx.ext.autodoc',      # Grabs documentation from inside modules
       'sphinx.ext.intersphinx'   # Link to other projects' documentation
   ]

Add ``autodoc`` options.  E.g.,

.. code-block:: python

   # -- Options for autodoc -----------------------------------------------------

   # https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html#confval-autodoc_default_options
   autodoc_default_options = \
       {
           'special-members': '__init__'
       }

Add Sphinx Source Files
^^^^^^^^^^^^^^^^^^^^^^^

As needed...

Modify Sphinx Root `index.rst`
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

As needed...

E.g.,

.. code-block:: restructuredtext

   =============================================
   Welcome to python-helloworld's documentation!
   =============================================

   Contents
   ========

   .. toctree::
      :maxdepth: 2

      user_guide/index
      developer_guide/index
      api/index

   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

Add Code
--------

As needed...

Lint Code
---------

To `lint <https://en.wikipedia.org/wiki/Lint_(software)>`_ the code, starting
in the repository directory, execute the command below.

.. code-block:: bash

   flake8 src

Commit Code
-----------

As needed...

E.g.,

.. code-block:: bash

   git status

   git add --dry-run .

   git add .

   git commit

Execute PyBuilder Tasks
-----------------------

To list available PyBuilder tasks, starting in the repository directory,
execute the command below.

.. code-block:: bash

   pyb --list-tasks

To list default PyBuilder tasks and task dependencies, starting in the
repository directory, execute the command below.

.. code-block:: bash

   pyb --list-plan-tasks

To list PyBuilder tasks and task dependencies that will be executed for a given
task, starting in the repository directory, execute the command below,
replacing ``TASK`` as needed.

.. code-block:: bash

   pyb --list-plan-tasks TASK

To execute default PyBuilder tasks and task dependencies, starting in the
repository directory, execute the command below.  See the `default_task`
variable in file `build.py`.

.. code-block:: bash

   pyb

Install a `target` Wheel File Using ``pipx``
--------------------------------------------

To install the current ``target`` wheel file using ``pipx``, starting from the
repository directory, execute the command below.

.. code-block:: bash

   pipx \
      install \
      --python $(pyenv which python3) \
      target/dist/*/dist/*.whl

To list packages installed using ``pipx``, execute the command below.

.. code-block:: bash

   pipx list

Any scripts under ``src/main/scripts`` should now be executable.  E.g.,

.. code-block:: bash

   which helloworld

   helloworld --help

   helloworld

   helloworld --greeting Hi --who universe

To uninstall the package, execute the command below, replacing ``PACKAGE`` as
needed.

.. code-block:: bash

   pipx uninstall PACKAGE
