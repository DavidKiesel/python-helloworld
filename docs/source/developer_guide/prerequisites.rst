Prerequisites
=============

``pyenv``
---------

``pyenv`` is a tool for a user to install and switch between multiple versions
of Python, including system versions and versions installed under the user's
home directory.  Files related to it are installed under directory
``$HOME/.pyenv``.

``pyenv-virtualenv`` is a ``pyenv`` plugin that provides features to manage
Python virtual environments and Conda environments.  Each virtual environment
is named by the user, is set to use a particular version of Python recognized
by ``pyenv``, and is stored under ``$HOME/.pyenv``.  Note that the correct
way to use the ``pyenv-virtualenv`` plugin is to use command ``pyenv
virtualenv``, not ``pyenv-virtualenv``.

References:

- `pyenv at Github <https://github.com/pyenv/pyenv>`_
- `pyenv-virtualenv at Github <https://github.com/pyenv/pyenv-virtualenv>`_

``pyenv`` Debian Install
^^^^^^^^^^^^^^^^^^^^^^^^

To install prerequisites for ``pyenv``, execute commands below.

.. code-block:: bash

   sudo apt-get \
       install \
       -y \
       build-essential \
       libssl-dev \
       zlib1g-dev \
       libbz2-dev \
       libreadline-dev \
       libsqlite3-dev \
       wget \
       curl \
       llvm \
       libncurses5-dev \
       libncursesw5-dev \
       xz-utils \
       tk-dev \
       libffi-dev \
       liblzma-dev \
       python-openssl \
       git

To install ``pyenv`` and ``pyenv-virtualenv``, execute commands below.

.. code-block:: bash

   curl https://pyenv.run | bash


Tested with Debian 10.6.

References:

- `pyenv installer <https://github.com/pyenv/pyenv-installer>`_
- `pyenv - Common build problems - Prerequisites <https://github.com/pyenv/pyenv/wiki/Common-build-problems#prerequisites>`_

``pyenv`` Mac OS Install
^^^^^^^^^^^^^^^^^^^^^^^^

To install ``pyenv`` and ``pyenv-virtualenv``, execute commands below.

.. code-block:: bash

   brew update

   brew install pyenv

   brew install pyenv-virtualenv

``pyenv`` Shell Activation
^^^^^^^^^^^^^^^^^^^^^^^^^^

Add the lines below to ``$HOME/.bash_profile``.

.. code-block:: bash

   ##############################################################################
   # set PYENV_ROOT

   PYENV_ROOT="${HOME}/.pyenv"

   ##############################################################################
   # if needed, add to PATH

   if [ -d "${PYENV_ROOT}/bin" ]
   then
       PATH="${PYENV_ROOT}/bin:${PATH}"
   fi

   if [ -d "${PYENV_ROOT}/plugins/pyenv-virtualenv/bin" ]
   then
       PATH="${PYENV_ROOT}/plugins/pyenv-virtualenv/bin:${PATH}"
   fi

   ##############################################################################
   # evals

   if command -v pyenv >/dev/null 2>&1
   then
       eval "$(pyenv init -)"
   fi

   if command -v pyenv-virtual-init >/dev/null 2>&1
   then
       eval "$(pyenv virtualenv-init -)"
   fi

``pipx``
--------

``pipx`` is a tool for installing and running Python applications in isolated
environments.

References:

- `pipx at pypi.org <https://pypi.org/project/pipx/>`_
- `pipx Homepage <https://github.com/pipxproject/pipx>`_
- `pipx Documentation <https://pipxproject.github.io/pipx/>`_

``pipx`` Debian Install
^^^^^^^^^^^^^^^^^^^^^^^

``pipx`` can be installed as a system package or under a user home directory.

To install the latest version of ``pipx`` under ``$HOME/.local``, execute
commands below.

.. code-block:: bash

   python3 -m pip install --user pipx

To install the system version of ``pipx``, execute commands below.

.. code-block:: bash

   sudo apt install pipx

``pipx`` Mac OS Install
^^^^^^^^^^^^^^^^^^^^^^^

To install ``pipx``, execute commands below.

.. code-block:: bash

   brew update

   brew install pipx

``pipx`` Shell Activation
^^^^^^^^^^^^^^^^^^^^^^^^^

Add the lines below to ``$HOME/.bash_profile``.

.. code-block:: bash

   ##############################################################################
   # if needed, add to PATH

   if [ -d "${HOME}/.local/bin" ]
   then
       PATH="${HOME}/.local/bin:${PATH}"
   fi

   ##############################################################################
   # evals

   if command -v register-python-argcomplete >/dev/null 2>&1
   then
       eval "$(register-python-argcomplete pipx)"
   fi

Sphinx
------

Sphinx is a software documentation generator.

References:

- `Sphinx at pypi.org <https://pypi.org/project/Sphinx/>`_
- `Sphinx Homepage <https://www.sphinx-doc.org/en/master/>`_

Sphinx Install
^^^^^^^^^^^^^^

To install Sphinx, execute commands below.  The ``--python`` argument is set so
the application will execute using the ``pyenv`` global version of Python at
the time the command is executed.

.. code-block:: bash

   pipx \
       install \
       --python "$(pyenv root)/versions/$(pyenv global)/bin/python" \
       sphinx

PyBuilder
---------

PyBuilder (``pyb``) is a software build automation tool for Python similar to
Apache Maven for Java.

References:

- `PyBuilder at pypi.org <https://pypi.org/project/pybuilder/>`_
- `PyBuilder Homepage <https://pybuilder.io/>`_

PyBuilder Install
^^^^^^^^^^^^^^^^^

To install PyBuilder, execute commands below.  The ``--python`` argument is set
so the application will execute using the ``pyenv`` global version of Python at
the time the command is executed.

.. code-block:: bash

   pipx \
       install \
       --python "$(pyenv root)/versions/$(pyenv global)/bin/python" \
       pybuilder

Flake8
------

Flake8 is a Python `linter <https://en.wikipedia.org/wiki/Lint_(software)>`_.
Note that the Flake8 plug-in for PyBuilder executes as part of the standard
``analyze`` task, which executes after the ``run_unit_tests`` task.  So, it can
be useful for troubleshooting code to execute Flake8 separately, prior to
executing PyBuilder.

References:

- `Flake8 at pypi.org <https://pypi.org/project/flake8/>`_
- `Flake8 Homepage <https://gitlab.com/pycqa/flake8>`_

Flake8 Install
^^^^^^^^^^^^^^

To install Flake8, execute commands below.  The ``--python`` argument is set so
the application will execute using the ``pyenv`` global version of Python at
the time the command is executed.

.. code-block:: bash

   pipx \
       install \
       --python "$(pyenv root)/versions/$(pyenv global)/bin/python" \
       flake8

Twine
-----

Twine is a utility for publishing Python packages on PyPI or a similar artifact
repository.

References:

- `Twine at pypi.org <https://pypi.org/project/twine/>`_
- `Twine Homepage <https://twine.readthedocs.io/en/latest/>`_
- `Python Packaging Tutorial <https://packaging.python.org/tutorials/packaging-projects/>`_ 

Twine Install
^^^^^^^^^^^^^

To install Twine, execute commands below.  The ``--python`` argument is set so
the application will execute using the ``pyenv`` global version of Python at
the time the command is executed.

.. code-block:: bash

   pipx \
       install \
       --python "$(pyenv root)/versions/$(pyenv global)/bin/python" \
       twine
